<?php
require_once("../../vendor/autoload.php");

$objStudent = new \App\Student();
$objCourse = new \App\Course();


$objStudent->setName($_POST['name']);
$objStudent->setSeip($_POST['seip']);



$objCourse->setMarkBangla($_POST['markBangla']);
$objCourse->setMarkMath($_POST['markMath']);
$objCourse->setMarkEnglish($_POST['markEnglish']);



$objCourse->setGradeBangla();
$objCourse->setGradeMath();
$objCourse->setGradeEnglish();


$markBangla = $objCourse->getMarkBangla();
$markMath = $objCourse->getMarkMath();
$markEnglish = $objCourse->getMarkEnglish();

$gradeBangla = $objCourse->getGradeBangla();
$gradeMath = $objCourse->getGradeMath();
$gradeEnglish = $objCourse->getGradeEnglish();


$name = $objStudent->getName();
$id = $objStudent->getSeip();

$result = <<<saveResult
<table border='1' width='500px' style='margin: 10px auto'>
        <tr>
            <td>Name:</td>
            <td>$name</td>
        </tr>
        <tr>
            <td>Student ID:</td>
            <td>$id</td>
        </tr>
    </table>

    <table border='1' width='500px' style='margin: 0 auto'>
        <tr>
            <td>Subject</td>
            <td>Marks</td>
            <td>Grade</td>
        </tr>
        <tr>
            <td>Bangla</td>
            <td>$markBangla</td>
            <td>$gradeBangla</td>
        </tr>
        <tr>
            <td>Math</td>
            <td>$markMath</td>
            <td>$gradeMath</td>
        </tr>
        <tr>
            <td>English</td>
            <td>$markEnglish</td>
            <td>$gradeEnglish</td>
        </tr>

    </table>
saveResult;



if (isset($_POST['submitForm'])) {
  try {

        if (empty($_POST['name'])) {
            throw new Exception("Name can not be Empty");
        }

        if (empty($_POST['seip'])) {
            throw new Exception("SEIP can not be Empty");
        }

        if (empty($_POST['markBangla'])) {
            throw new Exception("MarkBangla can not be Empty");       
        }

        if (empty($_POST['markMath'])) {
            throw new Exception("MarkMath can not be Empty");       
        }

        if (empty($_POST['markEnglish'])) {
            throw new Exception("MarkEnglish can not be Empty");       
        }

        file_put_contents("result.html", $result, FILE_APPEND);


    } catch(Exception $e) {
        $error_msg = $e->getMessage();
    }
} else {
    header("Location: InformationCollectionForm.php");
}

echo file_get_contents("result.html");






