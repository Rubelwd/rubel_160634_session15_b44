<?php

namespace App;


class Student
{

    private $name;
    private $seip;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setSeip($seip)
    {
        $this->seip = $seip;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSeip()
    {
        return $this->seip;
    }

}