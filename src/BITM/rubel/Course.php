<?php

namespace App;


class Course
{
    private $markBangla;
    private $gradeBangla;

    private $markMath;
    private $gradeMath;

    private $markEnglish;
    private $gradeEnglish;


    public function setMarkBangla($markBangla) {
        $this -> markBangla = $markBangla;

    }

    public function setGradeBangla() {
        $this ->gradeBangla = $this->markToGrade($this->markBangla);
    }

    public function setMarkEnglish($markEnglish)
    {
        $this->markEnglish = $markEnglish;

    }

    public function setGradeEnglish() {
        $this->gradeEnglish = $this->markToGrade($this->markEnglish);
    }

    public function setMarkMath($markMath)
    {
        $this->markMath = $markMath;
    }

    public function setGradeMath() {
        $this->gradeMath = $this->markToGrade($this->markMath);
    }

    public function getMarkBangla()
    {
        return $this->markBangla;
    }

    public function getMarkEnglish()
    {
        return $this->markEnglish;
    }

    public function getMarkMath()
    {
        return $this->markMath;
    }

    public function getGradeBangla()
    {
        return $this->gradeBangla;
    }

    public function getGradeMath()
    {
        return $this->gradeMath;
    }

    public function getGradeEnglish()
    {
        return $this->gradeEnglish;
    }


    public function markToGrade($mark) {
        switch($mark) {
            case ($mark > 79):
                $grade = "A+";
                break;
            case ($mark > 69):
                $grade = "A";
                break;
            case ($mark > 59):
                $grade = "A-";
                break;
            case ($mark > 49):
                $grade = "B";
                break;
            case ($mark > 39):
                $grade = "C";
                break;
            case ($mark > 32):
                $grade = "D";
                break;
            case ($mark < 32):
                $grade = "F";
                break;
                
            default: $grade = "F";    
        }
        return $grade;
    }



}